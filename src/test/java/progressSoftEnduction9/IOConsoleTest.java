package progressSoftEnduction9;

import java.io.IOException;

public class IOConsoleTest {

    public static void main(String[] args) throws IOException {
        DataBase dataBase = new InMemoryDatabase();
        UserManagement us = new UserManagement(dataBase);
        UserOption Console = new UserOption(us);

        Console.getMenuChoice();
    }
}
