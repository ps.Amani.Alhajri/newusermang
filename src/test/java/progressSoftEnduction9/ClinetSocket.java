package progressSoftEnduction9;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClinetSocket {
    public static void main(String[] args){
        try(Socket soc = new Socket("www.google.com", 80)) {
            System.out.println("Connected:  "+ soc.getLocalPort());
            soc.setSoTimeout(2000); // اذا مرت ثانيتين وما حصل شي يفقع
            InputStream is = soc.getInputStream();
            OutputStream os = soc.getOutputStream();
            PrintWriter pw = new PrintWriter(os);
            pw.println("GET / HTTP/1.1");
            pw.println("Host: www.google.com");
            pw.println(); // لازم تكون معك برنت فاضي لاين فاضي عشال يفهم السرفر انك خلصت كل حاجه
            pw.flush();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            String line;
            while((line = reader.readLine()) != null){
                System.out.println(line);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
