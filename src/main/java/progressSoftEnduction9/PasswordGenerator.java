package progressSoftEnduction9;

public class PasswordGenerator {

    final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    final String DIGITS = "0123456789";

    public String getPassword() {
        return generatePassword();
    }

    private String generatePassword() {
        // TODO you can have one method to do the same thing for letters and digits -> DONE
        String random5Digits = getRandomChar(5, DIGITS);
        String random3letters = getRandomChar(3, CHARACTERS);
        return getRandomChar(8, random5Digits + random3letters);
    }

    private String getRandomChar(int numberOfChar, String selection) {
        char[] letters = new char[numberOfChar];
        for (int i = 0; i < numberOfChar; i++) {
            int index = (int) (selection.length() * Math.random());
            char randChar = selection.charAt(index);
            selection = selection.substring(0, index) + selection.substring(index + 1);
            letters[i] = randChar;
        }
        return String.copyValueOf(letters);
    }

}
