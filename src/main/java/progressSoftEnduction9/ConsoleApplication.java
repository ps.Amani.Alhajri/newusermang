package progressSoftEnduction9;

import java.util.ArrayList;
import java.util.Scanner;
import java.lang.reflect.Method;


public class ConsoleApplication{

    private UserManagement userManagementSystem;

    ConsoleApplication(UserManagement userManagementSystem) {
        this.userManagementSystem = userManagementSystem;
    }



    public void displayAllUsers() {
        System.out.println("+---------------+--------------+----------------+--------------+");
        System.out.println(String.format("|%-15s|%-15s|%-15s|%-15s|", "Username", "Name", "E-Mail", "Active (yes/no)"));
        handleErrors(new Action(){ public void execute() throws IllegalArgumentException {
            System.out.println(getUserListToPrint());
        }});
        System.out.println("+---------------+--------------+----------------+--------------+");
    }



    public void getNewUserInfo() {
        String userInfo = promptUserForInfo();
        handleErrors(new Action(){ public void execute() throws IllegalArgumentException {
            UserInfo user = userManagementSystem.addNewUser(userInfo);
            printUserInfo(user);
        }});
    }

    public void printUserInfo(UserInfo user) {
        System.out.println("Name: " + user.getName());
        System.out.println("Username: " + user.getUsername());
        System.out.println("E-Mail: " + user.geteMail());
        System.out.println("Password: " + user.getPassword());
    }

    public void promptUserNameToEnable() {
        String username = getUsername();
        handleErrors(new Action(){ public void execute() throws IllegalArgumentException {
            userManagementSystem.enableUser(username);
        }});

    }

    public void promptUserNameToDisable() {
        String username = getUsername();
        // TODO exception handling for all usermanagement methods is duplicate -> DONE
        handleErrors(new Action(){ public void execute() throws IllegalArgumentException {
            userManagementSystem.disableUser(username);
        }});


    }

    public void promptUserNameToResetPassword() {
        String username = getUsername();
        handleErrors(new Action(){ public void execute() throws IllegalArgumentException {
            String newPassword = userManagementSystem.resetPassword(username);
            System.out.println("new Password: " + newPassword);
        }});

    }

    private String getUserListToPrint() {
        ArrayList<UserInfo> usersList = userManagementSystem.getAllUsers();
        String userList = "";
        for (UserInfo userInfo : usersList) userList += userInfo + "\n";
        return userList;
    }

    private String promptUserForInfo() {
        System.out.println("Enter user information in the format (username, name, email)");
        Scanner userInput = new Scanner(System.in);
        return userInput.nextLine();
    }

    private String getUsername() {
        System.out.println("Please enter the username: ");
        Scanner userInput = new Scanner(System.in);
        return userInput.nextLine();
    }

    public interface Action {
        void execute() throws IllegalArgumentException;
    }

    private static void handleErrors(Action action) throws IllegalArgumentException {
        try {
            action.execute();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

}


