package progressSoftEnduction9;

public interface Condition {
    boolean doCopy(String username, UserInfo user);
}
