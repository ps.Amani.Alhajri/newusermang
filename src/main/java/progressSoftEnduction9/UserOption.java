package progressSoftEnduction9;

import java.util.Scanner;

public class UserOption extends ConsoleApplication {


    UserOption(UserManagement userManagementSystem) {
        super(userManagementSystem);
    }

    public void getMenuChoice() {
        char choice;
        do {
            printMenu();
            Scanner userInput = new Scanner(System.in);
            choice = userInput.next().charAt(0);
        } while (checkUserInput(choice));
    }


    // TODO an option consist of a number, description, and an action to be executed when chosen ->DONE
    // try to encapsulate those properties in one class
    private void printMenu() {
        System.out.println("1- Display all users\n" +
                "2- Add new user\n" +
                "3- Enable user\n" +
                "4- Disable user\n" +
                "5- Reset Password\n" +
                "6- Exit\n" +
                "Please enter your option:");
    }

    public boolean checkUserInput(char choice) {
        switch (choice) {
            case '1':
                displayAllUsers();
                return true;
            case '2':
                getNewUserInfo();
                return true;
            case '3':
                promptUserNameToEnable();
                return true;
            case '4':
                promptUserNameToDisable();
                return true;
            case '5':
                promptUserNameToResetPassword();
                return true;
            case '6':
                return false;
        }
        throw new IllegalArgumentException("invalid input, You have to choose from 1-6!");
    }
}
