package progressSoftEnduction9;

import org.apache.commons.validator.routines.EmailValidator;

import java.util.ArrayList;

public class UserManagement {

    private DataBase dataBase;
    private final char USERNAME = 'U';
    private final char EMAIL = 'E';

    public UserManagement(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    public ArrayList<UserInfo> getAllUsers() {
        // TODO should return a list of users --> DONE
        if (dataBase.isEmptyDB())
            throw new IllegalArgumentException("Empty list");
        return dataBase.printUserList();
    }

    public UserInfo addNewUser(String newUser) {
        throwIfNull(newUser);
        String[] userInfo = newUser.toLowerCase().split(",");
        verifyRecord(userInfo);
        UserInfo user = createUserEntity(userInfo);
        storeTheData(user);
        return user;
    }

    public void enableUser(String username) {
        UserInfo userData = createUserEntity(username);
        enableOrDisable(userData, true);
    }

    public void disableUser(String username) {
        UserInfo userData = createUserEntity(username);
        enableOrDisable(userData, false);
    }

    public String resetPassword(String username) {
        UserInfo userData = createUserEntity(username);
        return setNewPassword(userData);
    }

    private void verifyRecord(String[] userInfo) {
        throwIfNotComplete(userInfo);
        throwIfInvalidEmailFormat(userInfo[2]);
        throwIfExists(userInfo);
    }

    private void throwIfNull(String userInput) {
        if (userInput == null)
            throw new NullPointerException("null user data");
    }

    private void throwIfInvalidEmailFormat(String email) {
        EmailValidator validator = EmailValidator.getInstance();
        if (!(validator.isValid(email)))
            throw new IllegalArgumentException("invalid email formatting!");
    }

    private void throwIfExists(String[] userInfo) {
        validateUsernameIfUnique(userInfo[0].replaceAll("\\s", ""));
         validateEmailIfUnique(userInfo[2].replaceAll("\\s", ""));
    }

    // TODO both validate methods have similar code --> DONE
    private void validateUsernameIfUnique(String Username) {
        Condition condition = getCondition(USERNAME);
        checkUserExistence(Username, condition, USERNAME);
    }

    private void validateEmailIfUnique(String email) {
        Condition condition = getCondition(EMAIL);
        checkUserExistence(email, condition, EMAIL);
    }

    private void checkUserExistence(String dataToCheck, Condition condition, char type) {
        // TODO you should return false or change the method to be void and throw exception
        UserInfo userData = dataBase.searchForUser(dataToCheck, condition);
        if(userData == null)
            return;
        if (type == 'E')
            throw new IllegalArgumentException("email already exist!");
        else if (type == 'U')
            throw new IllegalArgumentException("user already exist!");
    }


    private void throwIfNotComplete(String[] userInfo) {
        if (userInfo.length < 3)
            throw new IllegalArgumentException("invalid input formatting!");
    }

    private void storeTheData(UserInfo user) {
        dataBase.storeData(user);
    }

    private UserInfo createUserEntity(String[] userInfo) {
        String username = userInfo[0];
        String name = userInfo[1];
        String email = userInfo[2];
        return new UserInfo(username, name, email);
    }


    private UserInfo createUserEntity(String username) {
        throwIfNull(username);
        Condition condition = getCondition(USERNAME);
        return dataBase.searchForUser(username.toLowerCase(), condition);
    }

    private Condition getCondition(char copyType) {
        switch (copyType) {
            case 'U':
                return this::checkUserData;
            case 'E':
                return this::checkEmail;
        }
        // TODO Null object design pattern- DONE
        Condition failingCondition = (username, user) -> {
            throw new IllegalArgumentException("unknown copy type: " + copyType);
        };
        return failingCondition;
    }

    private boolean checkUserData(String username, UserInfo user) {
        return user.getUsername().equals(username);
    }

    private boolean checkEmail(String email, UserInfo user) {
        return user.geteMail().equals(email);
    }

    private void enableOrDisable(UserInfo user, boolean enableOrDisableFlag) {
        if (user == null)
            throw new IllegalArgumentException("username does not exist!");
        if (user.getActive() && enableOrDisableFlag)
            throw new IllegalArgumentException("user already active");
        if (!(user.getActive()) && !enableOrDisableFlag)
            throw new IllegalArgumentException("user already inactive");

        user.setActive(enableOrDisableFlag);
        dataBase.updateData(user);
    }


    private String setNewPassword(UserInfo user) {
        if (user == null)
            throw new IllegalArgumentException("username does not exist!");
        String oldPassword = user.getPassword();
        String newPassword;
        do {
            user.setPassword();
            newPassword = user.getPassword();
        } while (oldPassword.equals(newPassword));

        dataBase.updateData(user);
        return newPassword;
    }

}
